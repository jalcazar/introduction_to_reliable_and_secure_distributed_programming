
# Introduction to Reliable and Secure Distributed Programming *Second Edition*

## by <br> Christian Cachin<br/> Rachid Guerraoui <br /> Luis Rodrigues

This book provides an introduction to distributed programming abstractions and presents the fundamental algorithms that implement them in several distributed en- vironments. The reader is given insight into the important problems of distributed computing and the main algorithmic techniques used to solve these problems. Through examples the reader can learn how these methods can be applied to build- ing distributed applications. The central theme of the book is the tolerance to uncertainty and adversarial influence in a distributed system, which may arise from network delays, faults, or even malicious attacks.

---
**2011**

**ISBN** 978-3-642-15259-7

**e-ISBN** 978-3-642-15260-3
**DOI** 10.1007/978-3-642-15260-3

**ACM Computing Classification (1998)** C.2, F.2, G.2

**[http://distributedprogramming.net](http://distributedprogramming.net)**

---

## Part 1
Establishes the common ground.

 In Chapter 1 *Introduction*, we motivate the need for distributed programming abstractions.
 
 In Chapter 2 *Basic Abstractions*, we present different kinds of assumptions about the underlying distributed environment. <br />
We introduce a family of distributed-system models for this purpose. Basically, a model describes the low-level abstractions on which more sophisticated ones are built. These include process and communication link abstractions. This chapter might be considered as a reference to other chapters.
 
## Part 2
 Each chapter is devoted to one problem:
 
 In Chapter 3 *Reliable Broadcast*, we introduce communication abstractions for distributed programming. They permit the broadcasting of a message to a group of processes and offer diverse reliability guarantees for delivering messages to the processes.
 
 In Chapter 4 *Shared Memory*, we discuss shared memory abstractions, which encapsulate simple forms of distributed storage objects, accessed by read and write operations. These could be files in a distributed storage system or registers in the memory of a multi-processor computer. We cover methods for reading and writing data values by clients, such that a value stored by a set of processes can later be retrieved, even if some of the processes crash, have erased the value, or report wrong data.
 
 In Chapter 5 *Concensus*, we address the consensus abstraction through which a set of processes can decide on a common value, based on values that the processes initially propose. They must reach the same decision despite faulty processes, which may have crashed or may even actively try to prevent the others from reaching a common decision.
 
 In Chapter 6 *Concensus Variants*, we consider variants of consensus, which are obtained by extend- ing or modifying the consensus abstraction according to the needs of important applications. This includes total-order broadcast, terminating reliable broadcast, (non-blocking) atomic commitment, group membership, and view-synchronous communication.
 
 
 <br />
 
 
[http://distributedprogramming.net](http://distributedprogramming.net)

