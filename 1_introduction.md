Classes of distributed algorithms:

1. *fail-stop*, designed under the assumption that processes can fail by crashing but the crashes can be reliably detected by all the other processes;

2. *fail-silent*, where process crashes can never be reliably detected;

3. *fail-noisy*, where processes can fail by crashing and the crashes can
be detected, but not always in an accurate manner (accuracy is only eventual);

4. *fail-recovery*, where processes can crash and later recover and still
participate in the algorithm;

5. *fail-arbitrary*, where processes can deviate arbitrarily from the protocol specification and act in malicious, adversarial ways; and

6. *randomized*, where in addition to the classes presented so far, processes may make probabilistic choices by using a source of randomness.

These classes are not disjoint, and it is important to notice that we do not give a solution from each class for every abstraction.

<br />

**Chapter notes**

* The idea of using *multiple, replicated processes for tolerating faults of individual processes* links together most algorithms presented in this book. This paradigm *can be traced back to the work on the Software-Implemented Fault Tolerance (SIFT) project in 1978*, which addressed the challenging problem of building a fault-tolerant computer for aircraft control 

* The *atomic commit problem was posed in the context of distributed databases by Gray (1978)*. Later Skeen (1981) introduced a variant of the problem that ensures also liveness. We describe the nonblocking atomic commit problem in Chap. 6.

* *State-machine replication and its relation total-order broadcast* are described in a survey of Schneider (1990). 
