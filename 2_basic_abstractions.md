**Liveness and safety properties**

*Liveness property* is a property of a distributed system execution such that, for any time *t*, there is some hope that the property can be satisfied at some time *t' ≥ t*. It is a property for which, quoting Cicero, “while there is life there is hope.”

*Safety property*: Basically, a safety property is a property of a distributed algorithm that can be violated at some time *t* and never be satisfied again after that time. Roughly speaking, safety properties state that the algorithm should not do anything wrong.

It is usually better, for modularity purposes, to separate the safety and liveness properties of an abstraction specification into disjoint classes. However, we will sometimes for the sake of conciseness consider properties that are neither pure liveness nor pure safety properties, but rather a union of both.

**Serializability:**
All transactions appear to execute one after the other, i.e., they are serializable; serializability is usually guaranteed through some *distributed locking scheme* or with some *optimistic concurrency control* mechanism.

Serializability is the traditional *I* or isolation, in ACID. If users' transactions each preserve application correctness (*C* or consistency, in ACID), a serializable execution also preserves correctness. Therefore, serializability is a mechanism for guaranteeing database correctness.

Unlike linearizability, serializability does not—by itself—impose any real-time constraints on the ordering of transactions. Serializability is also not composable. Serializability does not imply any kind of deterministic order—it simply requires that some equivalent serial execution exists. [Linearizability versus Serializability - Peter Bailis](http://www.bailis.org/blog/linearizability-versus-serializability/)

**Linearizability:** An execution is said to be linearizable if

 1. every read returns the last value written; and
 2. for any two operations *o* and *o'*, if *o* precedes *o'* in the actual execution, then *o* also appears before *o'* in the linearization.

Linearizability for read and write operations is synonymous with the term "atomic consistency" and is the "C" or "consistency" in Gilbert and Lynch's proof of the CAP Theorem. We say linearizability is composable (or "local") because, if operations on each object in a system are linearizable, then all operations in the system are linearizable.
 
 <br />
 
**Distributed algorithms**

A distributed algorithm consists of a distributed collection of automata, one per process. The automaton at a process regulates the way the process executes its computation steps, i.e., how it reacts to a message.

<br />


**Crashes**

The simplest way of failing for a process is when the process stops executing steps.

<br />

**Omissions**

when a process does not send (or receive) a message that it is supposed to send (or receive) according to its algorithm.

<br />

**Crashes with Recoveries**

Sometimes, the assumption that particular processes never crash is simply not plausible for certain distributed environments. For instance, assuming that a majority of the processes do not crash might simply be too strong, even if this should not happen only during the period until an algorithm execution terminates.
In this case, we say that a process is faulty if either the process crashes and never recovers or the process keeps infinitely often crashing and recovering.

<br />


**Eavesdropping Faults**

When a distributed system operates in an untrusted environment, some of its components may become exposed to an adversary or even fall under its control.

A message-authentication code (**MAC**) authenticates data between two entities. It is based on a shared symmetric key, which is known only to the sender and to the receiver of a message, but to nobody else. 

**Digital signatures**
when a process does not send (or receive) a message that it is supposed to send (or receive) according to its algorithm.

<br />

**Asynchronous System**
Assuming an asynchronous distributed system comes down to not making any timing assumption about processes and links. This is precisely the approach taken so far for defining process and link abstractions. That is, we did not assume that processes have access to any sort of physical clock, nor did we assume any bounds on processing or communication delays.
Even without access to physical clocks, it is still possible to measure the passage of time based on the transmission and delivery of messages, such that time is defined with respect to communication. Time measured in this way is called logical time, and the resulting notion of a clock is called a logical clock.The following algorithm can be used to measure logical time in an asynchronous distributed system:
1. Each process *p* keeps an integer called *logical clock lp*, initially 0.2. Whenever an event occurs at process *p*, the *logical clock lp* is incremented by one unit.3. When a process sends a message, it adds a timestamp to the message with the value of its logical clock at the moment the message is sent. The timestamp of an event *e* is denoted by *t(e)*.
<br />

**Synchronous System**
Assuming a synchronous system comes down to assuming the following properties:

1. *Synchronous computation*: There is a known upper bound on processing delays.

2. *Synchronous communication*: There is a known upper bound on message transmission delays. That is, the time period between the instant at which a message is sent and the instant at which the message is delivered by the destination process is smaller than this bound.


*Alternatively*
<br/>
a real-time clock provides an alternative way for synchronization among multiple processes, and a synchronous system is alternatively characterized as follows:

*Synchronous physical clocks*. Every process is equipped with a local physical clock. There is a known upper bound on the rate at which the local physical clock deviates from a global real-time clock.Note that such a global clock exists also in our universe, but merely as a fictional device to simplify the reasoning about steps taken by processes. This clock is not accessible to the processes and no algorithm can rely on it.
<br />

Not surprisingly, the major limitation of assuming a synchronous system model is the coverage of the model, i.e., the difficulty of building a system where the tim- ing assumptions hold with high probability.

<br />

**Partial Synchrony**

Generally, distributed systems appear to be synchronous. More precisely, for most systems that we know of, it is relatively easy to define physical time bounds that are respected most of the time. There are, however, periods where the timing assumptions do not hold, i.e., periods during which the system is asynchronous. These are periods where the network is overloaded, for instance, or some process has a shortage of memory that slows it down. 

*One way to capture partial synchrony is to assume that the timing assumptions only hold eventually, without stating when exactly.*

<br />

**Leader Election**

Often one may not need to detect which processes have failed, but rather need to identify one process that has not failed. This process may then act as the leader that coordinates some steps of a distributed algorithm, and in a sense it is trusted by the other processes to act as their leader.


<br />

**Distributed-System Model**

A combination of

 1. a process abstraction, 
 2. a link abstraction, and possibly 
 3. a failure-detector abstraction defines a distributed-system model.


**Analysing performance of dirstibuted algorithms**

When we present a distributed algorithm that implements a given abstraction, we analyze its cost mainly using two metrics:

1. *the number of messages* required to terminate an operation of the abstraction, and 

2. *the number of communication steps* required to terminate such an operation. 

  For some algorithms, we evaluate also

3. *its total communication size*, which is the sum of the lengths of all messages sent by an algorithm. It is measured in bits.

  When evaluating the performance of algorithms in a crash-recovery model, besides the number of communication steps and the number of messages, we also consider

4. *the number of accesses to stable storage* (or the “logging operations”).


Algorithms that have their performance go proportionally down when the number of failures increases are sometimes called 
*gracefully degrading algorithms*.


Performance measurements are often stated in Big-O Notation, which provides only an upper bound on the asymptotic behavior of the function when its argument grows larger; usually the argument is *N*, the number of processes in the system. More precisely, when a metric satisfies *O(g(N))* for a function *g*, it means that for all *N* bigger than some value *N0*, the measure is at most a constant times *g(N)* in absolute value. In this case, the metric is said to be “on the order of *g(N)*.” For instance, a complexity of *O(N)* is also called linear in *N*, and a complexity of *O(N2)* is called quadratic in *N*.

<br />

**Resilence**

The relation between the number f of potentially faulty processes and the total number N of processes in the system is generally called resilience.

<br />


**Quorums**

A recurring tool for designing fault-tolerant algorithms for a set of N processes are quorums. A quorum is a set of processes with special properties.A quorum in a system with N crash-fault process abstractions (according to thefail-stop, fail-noisy, fail-silent, or fail-recovery system model) is any majority ofprocesses, i.e., any set of more than N/2 processes (equivalently, any set of ⌈ N +1 ⌉ 2or more processes). Several algorithms rely on quorums and exploit the fact that every two quorums overlap in at least one process. Note that even if f < N/2 pro- cesses fail by crashing, there is always at least one quorum of noncrashed processes in such systems.

<br />


**Exercise 2.1:** Explain under which assumptions the *fail-recovery* and the *fail-silent* models are similar in (note that in both models any process can commit omission faults).

When processes crash, they lose the content of their volatile memory and they commit omissions. If we assume:

1. that processes do have stable storage and store every update of their state in stable storage and 

2. that processes are not aware they have crashed and recovered then the two models are similar.

<br />

**Exercise 2.2:** The perfect point-to-point links abstraction allows messages from one sender to arrive at a receiver in a different order 
than they were sent. Some applications rely on first-in first-out (FIFO) order communication, however.

Specify a FIFO-order perfect point-to-point links abstraction which ensures, in addition to the guarantees of perfect point-to-point links,
that messages are not reordered.


**FIFO perfect point-to-point link, instance *plfifo*.**

| Events   |           |  
|----------|-------------|
| *Request:\<plfifo, Send \| q, m\>* |  Requests to send message *m* to process *q*.| 
| *Indication:\<plfifo Deliver \| p,m\>* |    Delivers message *m* sent by process *p*.|
| **Properties** ||
|*PLFIFO1: Reliable delivery*|If a correct process *p* send a message *m* to a correct process *q*, then *q* eventually delivers *m*.|
|*PLFIFO2: No duplication*| No message is delivered by a process more than once.|
|*PLFIFO3: No creation*|A message *m* is delivered by *q* only after it has be sent to *q* by process *p*.|
| *PLFIFO4: FIFO order*| If some process sends message *m1* before sending *m2*, then no correct process delivers *m2* unless it has already delivered *m1*.|


<br / >

**Exercise 2.7**: In a *fail-stop model*, which of the following properties are safety properties?1. *Every process that crashes is eventually detected.*

   This is a **liveness** property; we can never exhibit a time *t* in some execution and state that the property is violated. There is always the hope that eventually the failure detector detects the crashes.
   
   <br />
   2. *No process is detected before it crashes.* 

  This is a **safety** property. If a process is detected at time *t* before it has crashed then the property is violated at time *t*.
  
  <br />
  3. *No two processes decide differently.* 

  This is also a **safety** property, because it can be violated at some time *t* and never be satisfied again.

  <br />
  4. *No two correct processes decide differently.* 

  Since a correct process is a process that never crashes and executes an infinite number of steps, the set of correct processes is known a priori. Therefore, this property is also a **safety** property: once two correct processes have decided differently in some partial execution, no matter how we extend the execution, the property would not be satisfied.

  <br />
  5. *Every correct process decides before t timeunits*.

  This is a **safety** property: it can be violated at some time during an execution, where all correct processes have executed *t* of their own steps. If violated at that time, there is no hope that it will be satisfied again.
  <br />


6. *If some correct process decides then every correct process decides.*

  This is a **liveness** property: there is always the hope that the property is satisfied. It is interesting to note that the property can actually be satisfied by having the processes not do anything. Hence, the intuition that a safety property is one that is satisfied by doing nothing may be misleading.The conclusion in the last property is often stated more explicitly as . . . then every correct process eventually decides. The presence of the word "eventually” usually allows one to identify liveness properties.<br />
**Chapter notes**

* The notions of safety and liveness were singled out by Alpern and Schneider (1985). It was shown that any property of a distributed system execution can be viewed as a composition of a liveness and a safety property.

* The term *fault* is sometimes reserved for the known or suspected cause of a *failure*; similarly, a failure may mean only the observable deviation of an abstraction from its specification.
 
* Lamport (1978) introduced the notions of causality and logical time; this is probably the most influential work in the area of distributed computing.

  A Lamport clock is simple. Each process maintains a counter using the following rules:

 - Whenever a process does work, increment the counter
 - Whenever a process sends a message, include the counter
 - When a message is received, set the counter to `max(local_counter, received_counter) + 1` [Distributed systems
for fun and profit: Chapter 3 Time and order.] (http://book.mixu.net/distsys/single-page.html#time)

 For any two events, *a* a and *b*, if there's any way that *a* could have influenced *b*, then the Lamport timestamp of *a* will be less than the Lamport timestamp of *b*. It's also possible to have two events where we can't say which came first; when that happens, it means that they couldn't have affected each other. If *a* and *b* can't have any effect on each other, then it doesn't matter which one came first. [Wikipedia Article: "Lamport timestamps"] (https://en.wikipedia.org/w/index.php?title=Lamport_timestamps&oldid=845598900).

  <br />

  Using *Spanner TrueTime*, all transactions receive a timestamp which is based on the actual (wall-clock) current time. This enables there to be a concept of “before” and “after” for two different transactions, even those that are processed by completely disjoint set of servers. The transaction with a lower timestamp is “before” the transaction with a higher timestamp. Obviously, there may be a small amount of skew across the clocks of the different servers. Therefore, Spanner utilizes the concept of an “uncertainty” window which is based on the maximum possible time skew across the clocks on the servers in the system. After completing their writes, transactions wait until after this uncertainty window has passed before they allow any client to see the data that they wrote. [NewSQL database systems are failing to guarantee consistency, and I blame Spanner](http://dbmsmusings.blogspot.com/2018/09/newsql-database-systems-are-failing-to.html)
* Pease, Shostak, and Lamport (1980) formulated the problem of agreement in the presence of faults, as a way to abstract the underlying problems encountered during the design of SIFT. A related but different abstraction for agreement was presented later by Lamport, Shostak, and Pease (1982), assuming that processes are subject to arbitrary faults. To motivate the question, the agreement problem was formulated in terms of a Byzantine army, commanded by multiple generals that communicated only by couriers, and where some limited number of generals might be conspiring with the enemy. The term “Byzantine” has been used ever since to denote faulty processes that deviate from their assigned program in malicious and adversarial ways.
* Algorithms that assume processes can only fail by crashing and that every process has accurate information about which processes have crashed have been considered by Schneider, Gries, and Schlichting (1984). They called such processes “fail-stop.” In later works, this system model has been formulated using the notion of a perfect failure detector.
* *FLP* Fischer, Lynch, and Paterson (1985) established the fundamental result that no deterministic algorithm solves the consensus problem in an asynchronous system, even if only one process fails and it can only do so by crashing.

 <br />
 
 This result means that there is no way to solve the consensus problem under a very minimal system model in a way that cannot be delayed forever. The argument is that if such an algorithm existed, then one could devise an execution of that algorithm in which it would remain undecided ("bivalent") for an arbitrary amount of time by delaying message delivery - which is allowed in the asynchronous system model. Thus, such an algorithm cannot exist.

 This impossibility result is important because it highlights that assuming the asynchronous system model leads to a tradeoff: algorithms that solve the consensus problem must either give up safety or liveness when the guarantees regarding bounds on message delivery do not hold. [The FLP impossibility result in Distributed systems
for fun and profit](http://book.mixu.net/distsys/single-page.html#the-flp-impossibility-result)


* Dwork, Lynch, and Stockmeyer (1988) introduced intermediate timing models that lie between the synchronous and the asynchronous model and showed how to solve consensus under these assumptions. Systems with such timing assumption have been called “partially synchronous.”
* The use of synchrony assumptions to build leasing mechanisms was explored by Gray and Cheriton (1989).
* The idea of stubborn communication links was proposed by Guerraoui, Oliveria, and Schiper (1998), as a pragmatic variant of perfect links for the fail-recovery model, yet at a higher level than fair-loss links (Lynch 1996).

* The notion of an unreliable failure detector was precisely defined by Guerraoui (2000). Algorithms that rely on such failure detectors have been called “indulgent” (Guerraoui 2000; Dutta and Guerraoui 2005; Guerraoui and Raynal2004).* Public-key cryptography and the concept of digital signatures were invented by Diffie and Hellman (1976). The first practical implementation of digital signatures and the most widely used one until today is the RSA algorithm, discovered shortly afterward (Rivest, Shamir, and Adleman 1978).
* Comprehensive information on implementing cryptographic primitives is given in the classic book of Menezes, van Oorschot, and Vanstone (1997). Modern cryptography is formalized using notions from complexity theory. Goldreich (2004) presents a thorough mathematical treatment of the field.
* In practice, the authenticated perfect links abstraction can be implemented by the TLS protocol on the Internet or by using so-called tunnels constructed with the secure shell (SSH) protocol. These protocols protect the confidentiality and integrity of transported messages; for providing authenticated links, encryption is not needed and might be turned off to improve performance.* Quorums have first been formalized to ensure consistency among the processes in a distributed system by Thomas (1979) and by Gifford (1979). Byzantine quorums were introduced by Malkhi and Reiter (1998).* Apart from the majority quorums considered here, there exist many other quorum-system constructions, which also ensure that every two quorums overlap in at least one process. They can replace the majority quorums in the algorithms in this book and sometimes also improve the performance of these algorithms (Naor and Wool 1998).